class ArticlesController < ApplicationController

	def index
		@articles = Article.all
		render layout: "user_header"
	end

	def show
		@article = Article.find(params[:id])

		render layout: "user_header"
	end

	def new
		@article = Article.new
		render layout: "user_header"
	end

	def create
		@article = Article.new(article_params)

		if @article.save
			redirect_to @article
		else
			render 'new', layout: "user_header"
		end
	end

	def edit
		@article = Article.find(params[:id])

		render layout: "user_header"
	end

	def update
		@article = Article.find(params[:id])

		if @article.update(article_params)
			redirect_to @article
		else
			render 'edit', layout: "user_header"
		end
	end

	def destroy
		@article = Article.find(params[:id])

		@article.destroy

		redirect_to articles_path
	end

	private
		def article_params
			params.require(:article).permit(:title, :text)
		end
end
