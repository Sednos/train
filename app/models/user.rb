class User < ApplicationRecord
	before_save :encrypt_password

  def encrypt_password
    if password.present?
      self.password = Digest::MD5.hexdigest(password)
    end
  end
  
	validates :login, presence: true
	validates :password, presence: true,
						length: { minimum: 6, maximum: 15}
	validates :email, presence: true
end
